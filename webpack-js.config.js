const path = require('path');

module.exports = {
    entry: {},
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'www/js'),
    }
}
<?php

declare(strict_types=1);

namespace App\Front\UI\Home;

use App\Front\TemplateVariables\Presenters\HomeTemplate;
use App\Front\Models\BasePresenter;

/**
 * @property-read HomeTemplate $template
 */

class HomePresenter extends BasePresenter
{
}

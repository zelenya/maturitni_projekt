<?php

declare(strict_types=1);

namespace App\Front\TemplateVariables\Presenters;

use Nette\Bridges\ApplicationLatte\Template;

class HomeTemplate extends Template
{
    public string $curak;
}